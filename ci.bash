#!/bin/bash
set -e
git pull origin master
NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 

nvm install
nvm use
npm install

sudo service 4chanstream-server restart
sudo service 4chanstream-4chan restart
sudo service 4chanstream-8chan restart
echo "all good"
