var { DateTime, Duration } = require('luxon')
const { replaceLinks } = require('./common')
const axios = require('axios')
const version = require('./package.json').version
const axiosVersion = require('./node_modules/axios/package.json').version
axios.defaults.headers.common['User-Agent'] = `memestream.club/${version} (nodejs; Linux x86_64) axios/${axiosVersion}` // be a
axios.defaults.headers.common['Referer'] = `memestream.club`

const redis = require('redis')
const pub = redis.createClient({ host: 'redis' })
const { getLogger } = require('./logger')
const logger = getLogger('worker.8chan')
var lastFetchTime = {}
var lastBoardIndex = -1
const mode = '8chan'

const hidden = process.env.HIDDEN_SERVICE === 'true'

process.on('unhandledRejection', up => {
  throw up
})
process.on('uncaughtException', up => {
  console.error(up)
  process.exit(1)
})

async function doShit() {
  const boards = (await axios.get('https://8ch.net/boards.json')).data.filter(b => b.active > 100)

  logger.info({ mode, msg: 'loaded-board-list', activeBoards: boards.length })

  setInterval(async () => {
    lastBoardIndex = (lastBoardIndex + 1) % boards.length
    const board = boards[lastBoardIndex].uri
    const lastTime = lastFetchTime[board] || DateTime.local().plus({ minute: -1 })

    lastFetchTime[board] = DateTime.local()
    var f = null

    try {
      f = await axios.get(`https://8ch.net/${board}/0.json`, {
        headers: {
          'If-Modified-Since': lastTime.toHTTP(),
        },
        timeout: 1000, // 1 sec
      })
    } catch (error) {
      logger.error({ mode, msg: 'error', error: error.toString() })
    }
    if (!f || !f.data.threads) {
      return
    }

    const imgDomain = hidden ? '/images' : 'https://images.memestream.club/'

    const posts = f.data.threads.reduce((newPosts, thread) => {
      return newPosts.concat(
        thread.posts
          .filter(p => p.time * 1000 >= lastTime.toMillis())
          .filter(p => p.com && p.com.length && p.ext && p.ext !== '.pdf' && p.ext !== '.webm')
          .map(f =>
            Object.assign(f, {
              source: '8chan',
              board,
              com: replaceLinks(f.com, `https://8ch.net/${board}/res/${f.no}.html`),
              ogImg: `https://media.8ch.net/file_store/${f.tim}${f.ext}`,
              image: f.tim ? `${imgDomain}/https://media.8ch.net/file_store/${f.tim}${f.ext}` : null,
              thumbImage: f.filename ? `${imgDomain}/https://media.8ch.net/file_store/${f.tim}s.jpg` : null,
              thread: `https://8ch.net/${board}/res/${f.no}.html`,
              post: `https://boards.4chan.org/${board}/thread/${f.resto}#p${f.no}`,
            })
          )
      )
    }, [])
    logger.info({ mode, msg: 'emitted-posts', posts: posts.length, board })
    pub.publish('8chan', JSON.stringify(posts))
  }, 1.5 * 1000)
}
doShit()

logger.info({ mode, msg: 'started', hidden })

// todo: stop interval + close redis on process exit
