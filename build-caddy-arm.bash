#!/bin/bash
set -e
docker build -t memestream/caddy:arm64 -f Dockerfile-caddy . && docker push memestream/caddy:arm64
docker manifest create memestream/caddy:arm memestream/caddy:arm64 --amend
docker manifest annotate --arch arm memestream/caddy:arm memestream/caddy:arm64
docker manifest push memestream/caddy:arm  -p
