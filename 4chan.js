var { DateTime, Duration } = require('luxon')
const { replaceLinks } = require('./common')
const axios = require('axios')
const version = require('./package.json').version
const axiosVersion = require('./node_modules/axios/package.json').version
axios.defaults.headers.common[
  'User-Agent'
] = `memestream.club/${version} (nodejs; Linux x86_64) axios/${axiosVersion}` // be a
axios.defaults.headers.common['Referer'] = `memestream.club`

const redis = require('redis')
const pub = redis.createClient({ host: 'redis' })
const { getLogger } = require('./logger')
const logger = getLogger('worker.4chan')

const hidden = process.env.HIDDEN_SERVICE === 'true'
const lastFetchTime = {}
let lastBoardIndex = -1
const mode = '4chan'
process.on('unhandledRejection', up => {
  throw up
})
process.on('uncaughtException', up => {
  console.error(up)
  process.exit(1)
})
async function doShit() {
  const boards = (await axios.get('https://a.4cdn.org/boards.json')).data.boards

  setInterval(async () => {
    lastBoardIndex = (lastBoardIndex + 1) % boards.length
    const board = boards[lastBoardIndex].board
    var lastTime = lastFetchTime[board] || DateTime.local().plus({ minute: -1 })

    lastFetchTime[board] = DateTime.local()
    var f = null

    try {
      f = await axios.get(`https://a.4cdn.org/${board}/1.json`, {
        headers: {
          'If-Modified-Since': lastTime.toHTTP(),
        },
        timeout: 1000, // 1 sec
      })
    } catch (e) {
      logger.error({ mode, msg: 'error', error: e.toString() })
    }
    if (!f || !f.data.threads) {
      return
    }

    const imgDomain = hidden ? '/images' : 'https://images.memestream.club'

    const posts = f.data.threads.reduce((newPosts, thread) => {
      return newPosts.concat(
        thread.posts
          .filter(p => p.tim >= lastTime.toMillis())
          .filter(
            p =>
              p.com &&
              p.com.length &&
              p.ext &&
              p.ext !== '.pdf' &&
              p.ext !== '.webm'
          )
          .map(f =>
            Object.assign(f, {
              source: '4chan',
              com: replaceLinks(
                f.com,
                `https://boards.4chan.org/${board}/thread/${f.resto}`
              ),
              board,
              ogImg: `https://i.4cdn.org/${board}/${f.tim}${f.ext}`,
              image: f.filename
                ? `${imgDomain}/https://i.4cdn.org/${board}/${f.tim}${f.ext}`
                : null,
              thumbImage: f.filename
                ? `${imgDomain}/https://i.4cdn.org/${board}/${f.tim}s.jpg`
                : null,
              thread: `https://boards.4chan.org/${board}/thread/${f.resto}`,
              post: `https://boards.4chan.org/${board}/thread/${f.resto}#p${
                f.no
              }`,
            })
          )
      )
    }, [])
    logger.info({
      mode,
      msg: 'emitted-posts',
      board,
      posts: posts.length,
    })
    pub.publish('4chan', JSON.stringify(posts))
  }, 1.5 * 1000)
}
doShit()

logger.info({ mode, msg: 'started', hidden })

// todo: stop interval + close redis on process exit
