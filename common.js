exports.replaceLinks = function replaceLinks(s, post) {
  return s.replace(/href="#/g, `href=${post}#`);
};
