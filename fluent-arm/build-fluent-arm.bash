#!/bin/bash
set -e
docker build -t memestream/fluent:amd64 .
docker push memestream/fluent:amd64
docker manifest create memestream/fluent:arm memestream/fluent:amd64 --amend
docker manifest annotate --arch arm memestream/fluent:arm memestream/fluent:amd64
docker manifest push memestream/fluent:arm -p
