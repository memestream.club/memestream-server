trying to exp build
- create :amd64 build
- create :arm manifest
- annotate :arm manifest with platform with arch=arm
- docker push both


docker build -t memestream/fluent:amd64 .
docker push memestream/fluent:amd64
docker manifest create memestream/fluent:arm memestream/fluent:amd64
docker manifest annotate --arch arm memestream/fluent:arm memestream/fluent:amd64
docker manifest inspect memestream/fluent:arm
docker manifest push memestream/fluent:arm
