var app = require('http').createServer(handler)
var io = require('socket.io')(app, {
  path: '/ws',
  serveClient: false,
  pingTimeout: 15000,
  pingInterval: 30000,
})
var fs = require('fs')
var { DateTime, Duration } = require('luxon')
var redis = require('redis')

const { getLogger } = require('./logger')
const logger = getLogger('server')
var sub = redis.createClient({ host: 'redis' })
const port = parseInt(process.env.PORT || '8099')
const host = process.env.HOST || 'localhost'
app.listen(port, host)
const mode = 'server'
logger.info({ mode, msg: 'started', port, host })

process.on('unhandledRejection', up => {
  throw up
})
process.on('uncaughtException', up => {
  console.error(up)
  process.exit(1)
})

function handler(req, res) {
  fs.readFile(__dirname + '/index.html', function(err, data) {
    if (err) {
      res.writeHead(500)
      return res.end('Error loading index.html')
    }

    res.writeHead(200)
    res.end(data)
  })
}
sub.subscribe('4chan')

let lastTenPosts = []

sub.on('message', function(channel, message) {
  if (channel === '4chan' || channel == '8chan') {
    const msg = JSON.parse(message)
    io.sockets.emit('4chan', msg)
    lastTenPosts = [...msg, ...lastTenPosts].slice(0, 10)
  } else {
    logger.warn(`unexpected message received on channel: ${channel} with: ${message}`)
  }
})

//io.origins(["https://memestream.club"]);

io.on('connection', async socket => {
  socket.emit('immediate', lastTenPosts)

  const connectedDateTime = DateTime.local()
  logger.info({ mode, msg: 'user-connected' })
  socket.on('disconnect', function() {
    const disconnectedDateTime = DateTime.local()
    const connectedDuration = Duration.fromMillis(disconnectedDateTime - connectedDateTime)
    logger.info({
      mode,
      msg: 'user-disconnected',
      durationMs: connectedDuration.valueOf(),
      duration: connectedDuration.toISO(),
    })
  })
})

// todo: on process exit, shutdown redis + sockets
