var winston = require('winston')
var fluentTransport = require('fluent-logger').support.winstonTransport()

/**
 * get an instance of the winston logger
 * configured to return with a fluent trasport
 * @param tag (string) es index name sub tag
 */
exports.getLogger = tag => {
  console.log(process.env.FLUENT_ADDRESS, process.env.FLUENT_PORT)
  var config = {
    host: process.env.FLUENT_ADDRESS || '127.0.0.1',
    port: process.env.FLUENT_PORT || 24224,
    timeout: 1.0,
    // requireAckResponse: true // Add this option to wait response from Fluentd certainly
  }
  return winston.createLogger({
    transports: [
      new fluentTransport(
        (process.env.HIDDEN_SERVICE ? 'hidden-' : '') +
          'memestream.club.' +
          tag,
        config
      ),
      new winston.transports.Console(),
    ],
  })
}
