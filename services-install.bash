#!/bin/bash
set -e
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

cp Caddyfile ../Caddyfile
sudo cp  4chanstream-8chan.service /etc/systemd/system/4chanstream-8chan.service
sudo cp 4chanstream-4chan.service /etc/systemd/system/4chanstream-4chan.service
sudo cp 4chanstream-server.service /etc/systemd/system/4chanstream-server.service
sudo cp imageproxy.service /etc/systemd/system/imageproxy.service
sudo cp caddy.service /etc/systemd/system/caddy.service
sudo systemctl daemon-reload 

sudo service 4chanstream-server restart
sudo service 4chanstream-4chan restart
sudo service 4chanstream-8chan restart
