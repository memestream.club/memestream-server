FROM node:10-alpine
#RUN apk --no-cache add wget
WORKDIR /app
COPY package*.json /app/
RUN npm install --production
COPY . /app
#VOLUME /build

EXPOSE 8099
ENV PORT=8099
ENV HOST=0.0.0.0
ENV HIDDEN_SERVICE=false
USER node
HEALTHCHECK --interval=30s --timeout=1s CMD wget localhost:8099/?ping=true -q -O/dev/null || exit 1
CMD ["node","index.js"]
